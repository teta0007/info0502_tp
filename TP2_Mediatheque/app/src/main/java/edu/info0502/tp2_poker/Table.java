package edu.info0502.tp2_poker;

import java.util.*;

/**
 * Représentation de la table de jeu
 */
public class Table {
    private List<Carte> cartesCommunes;
    private PaquetCartes paquet;
    private Joueur[] joueurs;

    /**
     * Représente la table de jeu 
     * @param joueurs Représente les joueurs de la partie
     */
    public Table(Joueur[] joueurs) {
        this.cartesCommunes = new ArrayList<>();
        this.paquet = new PaquetCartes();
        this.joueurs = joueurs;
    }

    /**
     * permet de distribuer les 2 cartes de la main privée
     */
    public void distribuerCartesCachees() {
        for (Joueur joueur : joueurs) {
            for (int i = 0; i < 2; i++) {
                joueur.AjouterCarte(paquet.TirerCarte());
            }
        }
    }

    /**
     * Permet de distribuer les 5 cartes de la maain communes
     */
    public void distribuerCartesCommunes() {
        for (int i = 0; i < 3; i++) {
            cartesCommunes.add(paquet.TirerCarte());
        }
        cartesCommunes.add(paquet.TirerCarte());
        cartesCommunes.add(paquet.TirerCarte());
    }

    /**
     * Affiche la liste des cartes communes du joueurs sur la table
     * @return Retourne une liste de cartes communes
     */
    public List<Carte> getCartesCommunes() {
        return cartesCommunes;
    }

    /**
     * Affiche la liste des joueurs
     * @return Retourne la liste des joueurs de la partie
     */
    public Joueur[] getJoueurs() {
        return joueurs;
    }

    /**
     * Retourne une représentation des cartes communes sur la table 
     */
    @Override
    public String toString() {
        return "Cartes communes : " + cartesCommunes;
    }
}

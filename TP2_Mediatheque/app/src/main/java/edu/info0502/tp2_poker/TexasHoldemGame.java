package edu.info0502.tp2_poker;

/**
 * Représente la classe d'une partie de Texas Hold'Em
 */
public class TexasHoldemGame {
    private Table table;
    private Joueur[] joueurs;

    /**
     * Constructeur de la partie Texas Hold'Em
     * @param joueurs La liste des joueurs de la partie
     */
    public TexasHoldemGame(Joueur[] joueurs) {
        this.joueurs = joueurs;
        this.table = new Table(joueurs);
    }

    /**
     * Réaliser les opérations principales afin de lancer une partie, soit la distribution des cartes, privées et communes
     */
    public void jouer() {
        table.distribuerCartesCachees();
        
        table.distribuerCartesCommunes();
        
        System.out.println(table);
        
        for (Joueur joueur : joueurs) {
            for (Carte carte : table.getCartesCommunes()) {
                joueur.AjouterCarte(carte);
            }
        }
        
        for (Joueur joueur : joueurs) {
            System.out.println(joueur);
        }
        
        Joueur gagnant = DeterminerGagnant(joueurs);
        System.out.println("Le gagnant est : " + gagnant.getNom());
    }

    /**
     * Permet de déterminer qui est le gagnant de la partie 
     * @param joueurs Prend en paramètre la liste des joueurs de la partie
     * @return Retourne le nom du gagnant
     */
    public Joueur DeterminerGagnant(Joueur[] joueurs) {
        Joueur gagnant = joueurs[0];
        for (Joueur joueur : joueurs) {
            if (joueur.EvaluerMain() > gagnant.EvaluerMain()) {
                gagnant = joueur;
            }
        }
        return gagnant;
    }
}

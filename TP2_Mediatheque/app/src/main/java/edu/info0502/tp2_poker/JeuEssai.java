package edu.info0502.tp2_poker;

import edu.info0502.tp2_poker.*;
import edu.info0502.tp2_poker.PaquetCartes;

/**
 * Classe de test
 */
public class JeuEssai {
    /**
     * Exécution des divers méthodes afin de pourvoir les tester avec la poker classique et le Texas Hold'Em
     * @param args
     */
    public static void main(String[] args){
        System.out.println("TÉTART Yann\nINFO0502 : Indrotuction à la programmation répartie!\n\nRéalisation de tests sur les divers classes\n");

        PaquetCartes paquet = new PaquetCartes();
        System.out.println("Le paquet vient d'être créé et initialisé avec un nombre de " + paquet +" cartes\n");

        paquet.melanger();
        System.out.println("Le paquet vient d'être mélangé \n");

        Talon talon = new Talon(52);
        System.out.println("Le talon vient d'être créé\n");

        Joueur[] joueurs = new Joueur[4];
        for(int i=0; i<joueurs.length; i++){
            joueurs[i] = new Joueur("Joueur "+(i+1));
        }
        System.out.println("Quatre joueurs viennent d'être créés\n");

        paquet.DistribuerCartes(joueurs, talon);

        // afficher les mains des joueurs
        for(Joueur joueur : joueurs){
            System.out.println(joueur);
        }

        Joueur gagant = paquet.DeterminerGagant(joueurs);
        System.out.println("Le gagnant de cette partie est : "+gagant);


        System.out.println("\n\nMise en place du test pour le Texas Hold Em\n");

        Joueur[] joueursTexasHoldEm=new Joueur[4];
        for(int i=0; i<joueursTexasHoldEm.length; i++){
            joueursTexasHoldEm[i] = new Joueur("Joueur "+(i+1));
        }
        System.out.println("Les 4 quatres joueurs viennent d'être créés");

        TexasHoldemGame jeu = new TexasHoldemGame(joueursTexasHoldEm);
        jeu.jouer();

    }
}

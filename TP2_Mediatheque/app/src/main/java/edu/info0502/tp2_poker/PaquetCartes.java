package edu.info0502.tp2_poker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Représentation d'un paquet de 52 cartes
 */
public class PaquetCartes{
    private List<Carte> cartes;

    
    /**
     * Crée un nouveau paquet de 52 cartes 
     */
    public PaquetCartes(){
        cartes = new ArrayList<>();
        for(Carte.Couleur couleur : Carte.Couleur.values()){
            for(Carte.Valeur valeur : Carte.Valeur.values()){
                cartes.add(new Carte(couleur, valeur));
            }
        }
    }

    /**
     * Mélange de paquet de carte
     */
    public void melanger(){
        Random random = new Random();
        for(int i =cartes.size() -1; i>0; i--){
            int carte_index=random.nextInt(i+1);
            Carte temp = cartes.get(carte_index);
            cartes.set(carte_index, cartes.get(i));
            cartes.set(i, temp);
        }
    }

    /**
     * Tire la carte du dessus du paquet
     * @return La carte tirée, si le paquet n est pas vide, sinon retour une carte null
     */
    public Carte TirerCarte(){
        if(!cartes.isEmpty()){
            return cartes.remove(0);
        }
        return null;
    }

    /**
     * Retourne le nombre de cartes restantes dans le paquet
     * @return Un entier qui correpond au nombre de cartes restantes dans le paquet
     */
    public int getNbCartes(){
        return cartes.size();
    }


    /**
     * Afin de gérer la distribution des cartes à tous les joueurs, la variable NbCartesParJoueurs permet de définir le nombre d ecartes à donner à chaque joueur
     * @param joueurs Représente tous les joueurs
     * @param talon Représente le talon actuel
     */
    public void DistribuerCartes(Joueur[] joueurs, Talon talon){
        int NbCartesParJoueurs = 5;

        for(int i=0; i<NbCartesParJoueurs; i++){
            for(Joueur joueur : joueurs){
                Carte carte = talon.TirerCarte();
                if(carte!=null){
                    joueur.AjouterCarte(carte);
                }
            }
        }
    }

    /**
     * Afin de pouvoir déterminer le gagnant d'une partie
     * @param joueurs Représente l'ensemble des joueurs de la partie
     * @return La méthode va retourner le nom du gagant
     */
    public Joueur DeterminerGagant(Joueur[] joueurs){
        Joueur gagnant = joueurs[0];
        for(Joueur joueur : joueurs){
            if(joueur.EvaluerMain() > gagnant.EvaluerMain()){
                gagnant=joueur;
            }
        }
        return gagnant;
    }

    /**
     * Retourne une représentation du nombre de carte restantes dans le paquet
     * @return Retourne un toString pour la représentation
     */
    @Override
    public String toString(){
        return "Le Paquet de Cartes{" + "nombre de cartes=" + cartes.size() + "}";
    }
}
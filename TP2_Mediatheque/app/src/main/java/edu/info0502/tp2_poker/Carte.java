package edu.info0502.tp2_poker;


/**
 * Représentation des cartes 
 */
public class Carte {

    private Couleur couleur;
    private Valeur valeur;

    /**
     * Énumération des couleurs possible pour une carte
     */
    public enum Couleur{
        Coeur, Pique, Carreau, Trefle
    }

    /**
     * Énumération des valeurs possibles pour une cartes
     */
    public enum Valeur{
        As, Roi,Dame, Valet, Dix, Neuf, Huit, Sept, Six, Cinq, Quatre, Trois, Deux
    }


/**
 * Création d'une carte
 * @param couleur La couleur représente Coeur, Pique, Carreau ou Trèfle
 * @param valeur La valeur de l'as, roi, jusqu'à 2
 */
    public Carte(Couleur couleur, Valeur valeur){
        this.couleur = couleur;
        this.valeur = valeur;
    }

    /**
     * Définition de la couleur de la carte
     * @param couleur Modifie la couleur d'un carte
     */
    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    /**
     * Définir la valeur d'une carte
     * @param valeur Modifier la valeur d'une carte
     */
    public void setValeur(Valeur valeur) {
        this.valeur = valeur;
    }

    /**
     * Obtenir la couleur d'une carte
     * @return Retour la couleur de la carte sous forme Couleur
     */
    public Couleur getCouleur() {
        return couleur;
    }

    /**
     * Obtenir la valeur d'une carte
     * @return Retour la valeur de la carte sous forme Valeur
     */
    public Valeur getValeur() {
        return valeur;
    }

    /**
     * Obtenir la représentation d'une carte
     * @return Retour la valeur et la couleur de la carte sous forme d'un string
     */
    @Override
    public String toString(){
        return valeur + " de " + couleur;
    }
    
}
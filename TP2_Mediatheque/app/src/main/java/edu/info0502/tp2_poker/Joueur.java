package edu.info0502.tp2_poker;

import java.util.*;

/**
 * Représentation d'un joueur
 */
public class Joueur {
    private  String nom;
    private List<Carte> main;
    private List<Carte> CartesPrivées;
    private int score;

    /**
     * Permet de créer un joueur, avec une nom, une main (les cartes privées et les cartes communes), ainsi qu'un score et les cartes privées
     * @param nom Le nom du joueur au format String
     */
    public Joueur(String nom){
        this.nom = nom;
        this.main = new ArrayList<>();
        this.CartesPrivées = new ArrayList<>();
        this.score=0;
    }

    /**
     * Permet d'ajouter une carte à la main de cartes privées
     * @param carte Les cartes privées au format carte
     */
    public void AjouterCartePrivée(Carte carte){
        this.CartesPrivées.add(carte);
    }

    /**
     * Permet d'attribuer le nom au joueur
     * @param nom Nom du joueur
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Permet de modifier la main du joueur
     * @param main Représente la main du joueur
     */
    public void setMain(List<Carte> main) {
        this.main = main;
    }

    /**
     * Retourne le nom du joueur
     * @return Retourne le nom au format String
     */
    public String getNom() {
        return nom;
    }

    /**
     * Retourne la main du joueur
     * @return Retourne la main du joueur au format Main
     */
    public List<Carte> getMain() {
        return main;
    }

    /**
     * Permet d'ajouter une carte à la main du joueur (cartes privées + cartes communes = main)
     * @param carte Ajouter la carte dans le main
     */
    public void AjouterCarte(Carte carte){
        this.main.add(carte);
    }

    /**
     * Permet de réaliser l'évaluation de main du joueur
     * @return Retourne le score de la main du joueur
     */
    public int EvaluerMain(){
        int score=0;
        for(Carte carte : main){
            score = Math.max(score, carte.getValeur().ordinal());
        }
        return score;
    }

    /**
     * Retourne le nom du joueur avec la main qui lui est associée
     */
    @Override
    public String toString(){
        return nom + " a pour main : " + main;
    }
}

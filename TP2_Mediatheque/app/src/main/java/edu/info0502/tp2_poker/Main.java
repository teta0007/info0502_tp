package edu.info0502.tp2_poker;

import edu.info0502.tp2_poker.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Représente une main de poker (5 cartes)
 */
public class Main{
    
    private List<Carte> cartes;

    /**
     * Créer une nouvelle main vide
     */
    public Main(){
        cartes = new ArrayList<>(5);
    }

    /**
     * Ajouter 1 carte à la main
     * @param carte La carte à jouer
     * @return True si la carte a été ajoutée, false si la main à déjà 5 cartes (main pleine)
     */
    public boolean AjouterCarte(Carte carte){
        if(cartes.size() <5){
            cartes.add(carte);
            return true;
        }
        return false;
    }


    /**
     * Afficher les cartes de la main
     * @return Retourne les cartes de la main sous forme de liste
     */
    public List<Carte> getCartes(){
        return new ArrayList<>(cartes);
    }


    /**
     * Affiche une représentation de la main actuelle
     * @return Une chaîne de la main actuelle
     */
    @Override
    public String toString(){
        return cartes.toString();
    }
}
package edu.info0502.tp2_poker;

import edu.info0502.tp2_poker.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Représentation du talon
 */
public class Talon{
    private List<Carte> cartes;

    /**
     * Création d'un nouveau talon, en fonction du nombre de paquet
     * @param NbPaquet Nombre de paquets à inclure dans le talon
     */
    public Talon(int NbPaquet){
        cartes = new ArrayList<>();

        for(int i=0;i< NbPaquet; i++){
            PaquetCartes paquet = new PaquetCartes();
            paquet.melanger();
            while(paquet.getNbCartes() >0){
                cartes.add(paquet.TirerCarte());
            }
        }
    }

    /**
     * Tire la carte du dessus du talon
     * @return La carte tirée, ou null dans le cas ou le talon est vide
     */
    public Carte TirerCarte(){
        if(!cartes.isEmpty()){
            return cartes.remove(0);
        }
        return null;
    }
}
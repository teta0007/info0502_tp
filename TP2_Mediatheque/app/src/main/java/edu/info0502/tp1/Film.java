package edu.info0502.tp1;

/* Class pour représenter un film dans la médiathèque */
public class Film extends Media {
    /* Nom du réalisateur */
    private String realisateur;
    /* Année de sortie du film */
    private int annee;

    /* Constructeur pas défaut pour initialisation un film */
    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    /**
     * Constructeur avec paramètres suivants :
     * @param titre titre du film
     * @param cote cote du film
     * @param note  note 
     * @param realisateur Nom du réalisateur
     * @param annee Année de sortie
     */
    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    /**
     * Construteur d'un copie
     * @param film prend en paramètre le film à copier
     */
    public Film(Film film) {
        super(film);
        this.realisateur = film.realisateur;
        this.annee = film.annee;
    }

    /**
     * Getter pour retourner le nom du réalisateur
     * @return realisateur Le nom du réalisateur
     */
    public String getRealisateur() {
        return realisateur;
    }


    /**
     * Setter modifier le nom du réalisateur
     * @param realisateur 
     */
    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    /**
     * Getter pour retourner l'année de sortie du film
     * @return annee
     */
    public int getAnnee() {
        return annee;
    }

    /**
     * Setter pour modifier l'année de sortie du film
     * @param annee
     */
    public void setAnnee(int annee) {
        this.annee = annee;
    }

    /**
     * Création d'une copie d'un film avant de la retourner
     * return instance de film
     */
    @Override
    public Film clone() {
        return new Film(this);
    }

    /**
     * Comparation d'un film avec un autre pour vérifier qu'il ne s'agisse pas d'un doublons
     * @param obj l'objet à metter pour réaliser la comparaison
     * @return un booleen, true si les objects sont identiques, sinon false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        Film film = (Film) obj;
        return annee == film.annee && realisateur.equals(film.realisateur);
    }

    /**
     * Retourne une représentation sous forme de chaine de caractères du film
     * @return chaine de caractères avec l'intégralité des attributs du film
     */
    @Override
    public String toString() {
        return "Film{" +
                "titre='" + getTitre() + '\'' +
                ", cote=" + getCote() +
                ", note=" + getNote() +
                ", realisateur='" + realisateur + '\'' +
                ", annee=" + annee +
                '}';
    }
}

package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
    private String proprietaire;
    private Vector<Media> medias;

    public Mediatheque() {
        this.proprietaire = "TÉTART Yann";
        this.medias = new Vector<>();
    }

    public Mediatheque(Mediatheque mediatheque) {
        this.proprietaire = mediatheque.proprietaire;
        this.medias = new Vector<>(mediatheque.medias);
    }

    public void add(Media media) {
        medias.add(media);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Mediatheque{proprietaire='").append(proprietaire).append("', medias=[\n");
        for (Media media : medias) {
            sb.append("  ").append(media).append("\n");
        }
        sb.append("]}");
        return sb.toString();
    }
}

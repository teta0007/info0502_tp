package edu.info0502.tp1;

public class Media {
    private String titre;
    private StringBuffer cote;
    private int note;
    private static String nom;

    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    public Media(Media media) {
        this.titre = media.titre;
        this.cote = new StringBuffer(media.cote);
        this.note = media.note;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(cote);
    }

    public void setCote(String cote) {
        this.cote = new StringBuffer(cote);
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public static void setNom(String nouveauNom) {
        nom = nouveauNom;
    }

    public static String getNom() {
        return nom;
    }

    @Override
    public Media clone() {
        return new Media(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return note == media.note &&
                titre.equals(media.titre) &&
                cote.toString().equals(media.cote.toString());
    }

    @Override
    public String toString() {
        return "Media{" +
                "titre='" + titre + '\'' +
                ", cote=" + cote +
                ", note=" + note +
                '}';
    }
}

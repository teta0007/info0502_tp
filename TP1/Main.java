public class Main {
    public static void main(String[] args) {
        Media.setNom("Ma Médiathèque");

        Livre livre1 = new Livre("La Nuit des temps", "LN001", 5, "René Barjavel", "978-2266133418");
        Film film1 = new Film("La Nuit des temps", "FN001", 4, "Gabriel Axel", 1971);

        Mediatheque mediatheque = new Mediatheque();
        mediatheque.add(livre1);
        mediatheque.add(film1);

        System.out.println(mediatheque);
    }
}

public class Livre extends Media {
    private String auteur;
    private String isbn;

    public Livre() {
        super();
        this.auteur = "";
        this.isbn = "";
    }

    public Livre(String titre, String cote, int note, String auteur, String isbn) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.isbn = isbn;
    }

    public Livre(Livre livre) {
        super(livre);
        this.auteur = livre.auteur;
        this.isbn = livre.isbn;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public Livre clone() {
        return new Livre(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        Livre livre = (Livre) obj;
        return auteur.equals(livre.auteur) && isbn.equals(livre.isbn);
    }

    @Override
    public String toString() {
        return "Livre{" +
                "titre='" + getTitre() + '\'' +
                ", cote=" + getCote() +
                ", note=" + getNote() +
                ", auteur='" + auteur + '\'' +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
